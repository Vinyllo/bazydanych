import pymysql.cursors
import subprocess

class Database:

    def __init__(self, name, connection):
        self.database_name = name
        self.connection = connection

    def create_database(self):
        with self.connection.cursor() as cursor:
            cursor.execute('SHOW DATABASES')
            if not any(x for x in cursor.fetchall() if self.database_name in x):
                cursor.execute(f'CREATE DATABASE {self.database_name}')
                print(f'Database "{self.database_name}" created.')
            else:
                print(f'Database "{self.database_name}" already exists.')

    def drop_database(self):
        with self.connection.cursor() as cursor:
            cursor.execute(f'DROP DATABASE {self.database_name}')
            print(f'Database "{self.database_name}" deleted.')

    def load_sql_script(self, name):
        command = f'mysql -p -u root {self.database_name} < {name}'
        subprocess.check_output(command, shell=True, encoding='utf-8', stdin=subprocess.PIPE, stderr=subprocess.PIPE)
        print(f'Script "{name}" loaded successfully to the "{self.database_name}" database.')

def create_connection(name='', cursorclass=pymysql.cursors.Cursor):
    return pymysql.connect(
        host = "localhost",
        user = "root",
        password = "koktajlik1",
        db=name,
        charset='utf8mb4',
        cursorclass=cursorclass
    )

#connection = create_connection()
name = "dziennik"
# base = Database(name, connection)
# #base.drop_database()
# base.create_database()
connection = create_connection(name)

#base.load_sql_script("dziennik_szkolny.sql")
print("here")
with connection.cursor() as cursor:
	cursor.execute("SELECT * FROM dane_nauczyciel")
print(cursor.fetchall()) # get
connection.close()
