import pymysql.cursors
import subprocess
import datetime
from base import Database
from base import create_connection


teachers_query = {"a": "Wyświetl członkow klasy",
				  "b": "Dodaj nową ocene",
				  "c": "Dodaj notatke do newslettera",
				  "d": "Pokaż wystawione oceny",
				  "e": "Dodanie uwagi",
				  "f": "Pokaż newsletter"
				  }

students_query = {"a": "Wypisz oceny",
				  "b": "Pokaż newsletter",
				  "c": "Pokaż członków klasy"
				  }

parents_query = {
				"a": "Pokaż newsletter",
				"b": "Podgląd uwag ucznia"
				}


def print_dict(_dict):
	for i,v in _dict.items():
		print(i,v)

class Interface():
	def __init__(self, queries):
		self.selected_query = ""
		self.queries = queries

	def chose(self):
		print("Wybierz zapytanie: ")
		print_dict(self.queries)
		self.selected_query = input()

class Parent(Interface):
	def __init__(self, surname):
		super(Parent, self).__init__(parents_query)
		self.surname = surname

	def select_query(self, base):
		if self.selected_query == "a":
			base.get_news
		elif



class Student(Interface):
	def __init__(self, surname, class_):
		super(Student, self).__init__(students_query)
		self.surname = surname
		self.class_ = class_

	def select_query(self, base):
		if self.selected_query == "a":
			base.get_grades(self.surname)
		elif self.selected_query == "b":
			base.get_news()
		elif self.selected_query == "c":
			base.print_selected_class(self.class_)

class Teacher(Interface):
	def __init__(self, name, surname):
		super(Teacher, self).__init__(teachers_query)
		self.name = name
		self.surname = surname

	def select_query(self, base):
		if self.selected_query == "a":
			selected_class=input("Wybierz klasę: a, b, c, d: ")
			base.print_selected_class(selected_class)

		elif self.selected_query == "b":
			base.add_grade(input("Wpisz ocene: "), self.name, self.surname, input("Klasa: "),input("Imię ucznia: "), input("Nazwisko ucznia: "), input("Przedmiot: "), input("Waga oceny: "), input("Komentarz: "))

		elif self.selected_query == "c":
			base.add_news(input("Wpisz newsa: "), self.name, self.surname, input("Klasa: "))

		elif self.selected_query == "d":
			base.get_faculty_grades(self.name, self.surname)

		elif self.selected_query == "e":
			base.add_raport(input("Imię ucznia: "), input("Nazwisko ucznia: "), input("Treść: "), self.name, self.surname)

		elif self.selected_query == "f":
			base.get_news()


class MainWindow():
	def __init__(self):
		connection = create_connection()
		self.base = Database("dziennik", connection)
		self.base.drop_database()
		self.base.load_sql_script("dziennik_szkolny.sql")
		self.base.create_database()

	def do_the_magic(self):
		print("Zaloguj się jako [N]auczyciel, lub [U]czeń: ")
		person = "N"#input()
		print(person)
		while True:
			if person == "U":
				acces = Student("Lis", "a")
				acces.chose()
				acces.select_query(self.base)
			elif person == "N":
				acces = Teacher("Kamil", "Mach")
				acces.chose()
				acces.select_query(self.base)
			elif person == "R":
				acces = Teacher("")
				acces.chose()
				acces.select_query(self.base)
			elif person == "O":
				return



MainWindow().do_the_magic()
#sudo systemctl start mariadb