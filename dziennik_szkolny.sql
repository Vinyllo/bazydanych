-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 14 Maj 2019, 00:24
-- Wersja serwera: 10.1.39-MariaDB
-- Wersja PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `ds`
--

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `aktywni_nauczyciele`
-- (Zobacz poniżej rzeczywisty widok)
--
CREATE TABLE `aktywni_nauczyciele` (
`id_nauczyciela` int(11)
,`imie_nauczyciel` text
,`nazwisko_nauczyciel` text
,`nr tel` int(11)
,`adres` text
,`specjalnosc` text
,`aktywny` tinyint(1)
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `aktywni_uczniowie`
-- (Zobacz poniżej rzeczywisty widok)
--
CREATE TABLE `aktywni_uczniowie` (
`imie_uczen` text
,`nazwisko_uczen` text
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dane_nauczyciel`
--

CREATE TABLE `dane_nauczyciel` (
  `id_nauczyciela` int(11) NOT NULL,
  `imie_nauczyciel` text COLLATE utf8_polish_ci NOT NULL,
  `nazwisko_nauczyciel` text COLLATE utf8_polish_ci NOT NULL,
  `nr tel` int(11) NOT NULL,
  `adres` text COLLATE utf8_polish_ci NOT NULL,
  `specjalnosc` text COLLATE utf8_polish_ci NOT NULL,
  `aktywny` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `dane_nauczyciel`
--

INSERT INTO `dane_nauczyciel` (`id_nauczyciela`, `imie_nauczyciel`, `nazwisko_nauczyciel`, `nr tel`, `adres`, `specjalnosc`, `aktywny`) VALUES
(1, 'Kamil', 'Mach', 50233232, 'Prusa 13', 'Matematyka', 1),
(2, 'Adrian', 'Konar', 12312322, 'Paprotna 12', 'Fizyka', 1),
(3, 'Hubert', 'Machnacz', 12312390, 'Kcyni 3', 'Biologia', 1),
(4, 'Sebastian', 'Krzaczyński', 99099022, 'Zawrotna 123', 'Polski', 1),
(7, 'Kacper', 'Woliński', 909099092, 'Solnicza 13', 'WOS', 0),
(8, 'Piotr', 'Wolski', 904499011, 'Paprotna 8', 'Historia', 0),
(9, 'Anna', 'Milewska', 231842910, 'Błotna 3', 'Angielski', 1),
(10, 'Natalia', 'Bacławska', 827190219, 'Bliska 1', 'Niemiecki', 1),
(11, 'Milena', 'Guz', 657190219, 'Bogdańska 1', 'Plastyka', 1),
(12, 'Bogumiła', 'Nadworska', 927190219, 'Wrocławska 1', 'Geografia', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dane_rodzic`
--

CREATE TABLE `dane_rodzic` (
  `id_rodzica` int(11) NOT NULL,
  `imie_rodzica` text COLLATE utf8_polish_ci NOT NULL,
  `nazwisko_rodzica` text COLLATE utf8_polish_ci NOT NULL,
  `nr_tel_rodzica` int(11) NOT NULL,
  `adres` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `dane_rodzic`
--

INSERT INTO `dane_rodzic` (`id_rodzica`, `imie_rodzica`, `nazwisko_rodzica`, `nr_tel_rodzica`, `adres`) VALUES
(5, 'Mieczysław', 'Gąbka', 111111111, 'Bakłażana 15/7'),
(20, 'Adam', 'Boguc', 213827491, 'Kolorwa 1'),
(21, 'Kamil', 'Modlin', 492056873, 'Mickiewicza 3'),
(22, 'Marcin', 'Grec', 213827401, 'Zolta 1'),
(23, 'Antoni', 'Amil', 490056873, 'Niebieska 2'),
(24, 'Kornel', 'Kasza', 216827491, 'Zlota 5'),
(25, 'Maciek', 'Kowalczyk', 496056873, 'Srebrna 1'),
(26, 'Mateusz', 'Sroka', 213727491, 'Borowska 2'),
(27, 'Patryk', 'Kruk', 427056873, 'Mylna 2'),
(28, 'Emil', 'Akil', 213127491, 'Bogdanska 2'),
(29, 'Justyna', 'Alis', 412056873, 'Dluga 3'),
(30, 'Elzbieta', 'Lis', 213827491, 'Kolorwa 3'),
(31, 'Bozena', 'Konop', 492056873, 'Mickiewicza 1'),
(32, 'Gosia', 'Bod', 213827401, 'Zolta 7'),
(33, 'Agnieszka', 'Adik', 490056873, 'Niebieska 3'),
(34, 'Robert', 'Amon', 216827491, 'Zlota 9'),
(35, 'Jakub', 'Andre', 496056873, 'Srebrna 2'),
(36, 'Klaudia', 'Ksyal', 213727491, 'Borowska 4'),
(37, 'Paulina', 'Codin', 427056873, 'Mylna 7'),
(38, 'Mirela', 'Dodo', 213127491, 'Bogdanska 1'),
(39, 'Ania', 'Olin', 412056873, 'Dluga 9'),
(40, 'Leo', 'Messi', 731221743, 'Barcelońska 12');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dane_uczen`
--

CREATE TABLE `dane_uczen` (
  `id_ucznia` int(11) NOT NULL,
  `imie_uczen` text COLLATE utf8_polish_ci NOT NULL,
  `nazwisko_uczen` text COLLATE utf8_polish_ci NOT NULL,
  `nr_tel_ucznia` int(11) NOT NULL,
  `adres` text COLLATE utf8_polish_ci NOT NULL,
  `aktywny` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `dane_uczen`
--

INSERT INTO `dane_uczen` (`id_ucznia`, `imie_uczen`, `nazwisko_uczen`, `nr_tel_ucznia`, `adres`, `aktywny`) VALUES
(28, 'Grzegorz', 'Boguc', 213827491, 'Kolorwa 1', 1),
(29, 'Ania', 'Modlin', 492056873, 'Mickiewicza 3', 1),
(30, 'Agnieszka', 'Grec', 213827401, 'Zolta 1', 1),
(31, 'Wiktoria', 'Amil', 490056873, 'Niebieska 2', 1),
(32, 'Milena', 'Kasza', 216827491, 'Zlota 5', 1),
(33, 'Pawe│', 'Kowalczyk', 496056873, 'Srebrna 1', 1),
(34, 'Emil', 'Sroka', 213727491, 'Borowska 2', 1),
(35, 'Alan', 'Kruk', 427056873, 'Mylna 2', 1),
(36, 'Basia', 'Akil', 213127491, 'Bogdanska 2', 1),
(37, 'Beata', 'Alis', 412056873, 'Dluga 3', 1),
(38, 'Adam', 'Lis', 213827491, 'Kolorwa 3', 1),
(39, 'Kamil', 'Konop', 492056873, 'Mickiewicza 1', 0),
(40, 'Marcin', 'Bod', 213827401, 'Zolta 7', 1),
(41, 'Antoni', 'Adik', 490056873, 'Niebieska 3', 1),
(42, 'Kornel', 'Amon', 216827491, 'Zlota 9', 1),
(43, 'Maciek', 'Andre', 496056873, 'Srebrna 2', 1),
(44, 'Mateusz', 'Ksyal', 213727491, 'Borowska 4', 1),
(45, 'Patryk', 'Codin', 427056873, 'Mylna 7', 1),
(46, 'Emil', 'Dodo', 213127491, 'Bogdanska 1', 1),
(47, 'Justyna', 'Olin', 412056873, 'Dluga 9', 1),
(48, 'Marek', 'Gąbka', 749173973, 'Bakłażana 15/7', 1),
(50, 'Cristiano', 'Messi', 928164827, 'Barcelońska 12', 1);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `klasa_a`
-- (Zobacz poniżej rzeczywisty widok)
--
CREATE TABLE `klasa_a` (
`nazwa_klasy` text
,`rocznik` int(11)
,`imie_uczen` text
,`nazwisko_uczen` text
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `klasa_b`
-- (Zobacz poniżej rzeczywisty widok)
--
CREATE TABLE `klasa_b` (
`nazwa_klasy` text
,`rocznik` int(11)
,`imie_uczen` text
,`nazwisko_uczen` text
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `klasa_c`
-- (Zobacz poniżej rzeczywisty widok)
--
CREATE TABLE `klasa_c` (
`nazwa_klasy` text
,`rocznik` int(11)
,`imie_uczen` text
,`nazwisko_uczen` text
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `klasa_d`
-- (Zobacz poniżej rzeczywisty widok)
--
CREATE TABLE `klasa_d` (
`nazwa_klasy` text
,`rocznik` int(11)
,`imie_uczen` text
,`nazwisko_uczen` text
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klasa_przedmiot`
--

CREATE TABLE `klasa_przedmiot` (
  `id_klasa_przedmiot` int(11) NOT NULL,
  `id_klasy` int(11) NOT NULL,
  `id_przedmiotu` int(11) NOT NULL,
  `id_nauczyciela` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `klasa_przedmiot`
--

INSERT INTO `klasa_przedmiot` (`id_klasa_przedmiot`, `id_klasy`, `id_przedmiotu`, `id_nauczyciela`) VALUES
(1, 1, 1, 1),
(2, 1, 1, 1),
(3, 2, 1, 1),
(4, 3, 1, 1),
(5, 4, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klasy`
--

CREATE TABLE `klasy` (
  `id_klasy` int(11) NOT NULL,
  `nazwa_klasy` text COLLATE utf8_polish_ci NOT NULL,
  `id_wychowawcy` int(11) NOT NULL,
  `rocznik` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `klasy`
--

INSERT INTO `klasy` (`id_klasy`, `nazwa_klasy`, `id_wychowawcy`, `rocznik`) VALUES
(1, 'A', 1, 2019),
(2, 'B', 2, 2019),
(3, 'C', 3, 2019),
(4, 'D', 7, 2019);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `newsletter`
--

CREATE TABLE `newsletter` (
  `id_newsletteru` int(11) NOT NULL,
  `id_klasy` int(11) NOT NULL,
  `id_nauczyciela` int(11) NOT NULL,
  `tresc` text COLLATE utf8_polish_ci NOT NULL,
  `data` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `newsletter`
--

INSERT INTO `newsletter` (`id_newsletteru`, `id_klasy`, `id_nauczyciela`, `tresc`, `data`) VALUES
(1, 1, 2, 'Sprawdzian z matematyki.', '02.06.2019');

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `newsletter_tresc`
-- (Zobacz poniżej rzeczywisty widok)
--
CREATE TABLE `newsletter_tresc` (
`nazwa_klasy` text
,`tresc` text
,`data` text
,`imie_nauczyciel` text
,`nazwisko_nauczyciel` text
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `nieaktywni_nauczyciele`
-- (Zobacz poniżej rzeczywisty widok)
--
CREATE TABLE `nieaktywni_nauczyciele` (
`id_nauczyciela` int(11)
,`imie_nauczyciel` text
,`nazwisko_nauczyciel` text
,`nr tel` int(11)
,`adres` text
,`specjalnosc` text
,`aktywny` tinyint(1)
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `nieaktywni_uczniowie`
-- (Zobacz poniżej rzeczywisty widok)
--
CREATE TABLE `nieaktywni_uczniowie` (
`imie_uczen` text
,`nazwisko_uczen` text
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `oceny`
--

CREATE TABLE `oceny` (
  `id_oceny` int(11) NOT NULL,
  `id_ucznia` int(11) NOT NULL,
  `id_nauczyciela` int(11) NOT NULL,
  `id_przedmiotu` int(11) NOT NULL,
  `ocena` int(11) NOT NULL,
  `data_wystawienia` date NOT NULL,
  `waga` float NOT NULL,
  `komentarz` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `oceny`
--

INSERT INTO `oceny` (`id_oceny`, `id_ucznia`, `id_nauczyciela`, `id_przedmiotu`, `ocena`, `data_wystawienia`, `waga`, `komentarz`) VALUES
(1, 38, 1, 1, 5, '2019-04-08', 3, 'Sprawdzian'),
(2, 38, 1, 1, 4, '2019-04-01', 3, 'Sprawdzian'),
(3, 30, 1, 1, 4, '2019-05-31', 2, 'Kartkówka'),
(4, 30, 1, 1, 3, '2019-05-31', 3, 'Sprawdzian'),
(5, 31, 1, 1, 4, '2019-05-31', 2, 'Kartkówka'),
(6, 32, 1, 1, 5, '2019-05-31', 2, 'Kartkówka'),
(7, 33, 1, 1, 2, '2019-05-31', 2, 'Kartkówka'),
(8, 34, 1, 1, 3, '2019-05-31', 2, 'Kartkówka'),
(9, 35, 1, 1, 1, '2019-05-31', 2, 'Kartkówka'),
(10, 36, 1, 1, 4, '2019-05-31', 2, 'Kartkówka'),
(11, 43, 2, 2, 3, '2019-05-22', 3, 'Kartkówka');

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `oceny_uczen`
-- (Zobacz poniżej rzeczywisty widok)
--
CREATE TABLE `oceny_uczen` (
`imie_uczen` text
,`nazwisko_uczen` text
,`ocena` int(11)
,`waga` float
,`komentarz` text
,`nazwa` text
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `opiekunowie_uczniow`
-- (Zobacz poniżej rzeczywisty widok)
--
CREATE TABLE `opiekunowie_uczniow` (
`imie_uczen` text
,`nazwisko_uczen` text
,`imie_rodzica` text
,`nazwisko_rodzica` text
,`nr_tel_rodzica` int(11)
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przedmioty`
--

CREATE TABLE `przedmioty` (
  `id_przedmiotu` int(11) NOT NULL,
  `nazwa` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `przedmioty`
--

INSERT INTO `przedmioty` (`id_przedmiotu`, `nazwa`) VALUES
(1, 'Matematyka'),
(2, 'Fizyka'),
(3, 'Biologia'),
(4, 'Polski'),
(5, 'WOS'),
(6, 'Historia'),
(7, 'Angielski'),
(8, 'Niemiecki'),
(9, 'Plastyka'),
(10, 'Geografia');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uczen_klasa`
--

CREATE TABLE `uczen_klasa` (
  `id_uczen_klasa` int(11) NOT NULL,
  `id_ucznia` int(11) NOT NULL,
  `id_klasy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `uczen_klasa`
--

INSERT INTO `uczen_klasa` (`id_uczen_klasa`, `id_ucznia`, `id_klasy`) VALUES
(7, 38, 1),
(8, 28, 2),
(9, 29, 3),
(10, 30, 4),
(11, 31, 1),
(12, 32, 2),
(13, 33, 3),
(14, 34, 4),
(15, 35, 1),
(16, 36, 2),
(17, 37, 3),
(18, 39, 4),
(19, 40, 1),
(20, 41, 2),
(21, 42, 3),
(22, 43, 4),
(23, 44, 1),
(24, 45, 2),
(25, 45, 3),
(26, 47, 4),
(27, 48, 2),
(28, 50, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uczen_rodzic`
--

CREATE TABLE `uczen_rodzic` (
  `id_wspolne` int(11) NOT NULL,
  `id_rodzica` int(11) NOT NULL,
  `id_ucznia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `uczen_rodzic`
--

INSERT INTO `uczen_rodzic` (`id_wspolne`, `id_rodzica`, `id_ucznia`) VALUES
(7, 5, 48),
(8, 20, 28),
(9, 21, 29),
(10, 22, 30),
(11, 23, 31),
(12, 24, 32),
(13, 25, 33),
(14, 26, 34),
(15, 27, 35),
(16, 28, 36),
(17, 29, 37),
(18, 30, 38),
(19, 31, 39),
(20, 32, 40),
(21, 33, 41),
(22, 34, 42),
(23, 35, 43),
(24, 36, 44),
(25, 37, 45),
(26, 38, 46),
(27, 39, 47),
(28, 40, 50);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uwagi`
--

CREATE TABLE `uwagi` (
  `id_uwagi` int(11) NOT NULL,
  `id_nauczyciela` int(11) NOT NULL,
  `id_ucznia` int(11) NOT NULL,
  `tresc` text COLLATE utf8_polish_ci NOT NULL,
  `data_wystawienia` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `uwagi`
--

INSERT INTO `uwagi` (`id_uwagi`, `id_nauczyciela`, `id_ucznia`, `tresc`, `data_wystawienia`) VALUES
(4, 2, 31, 'Przeszkadzał w prowadzeniu zajęć.', '2019-05-22'),
(6, 8, 36, 'Zapomniała zeszytu i podręcznika.', '2019-05-07');

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `uwagi_z_danymi`
-- (Zobacz poniżej rzeczywisty widok)
--
CREATE TABLE `uwagi_z_danymi` (
`imie_uczen` text
,`nazwisko_uczen` text
,`tresc` text
,`data_wystawienia` date
,`imie_nauczyciel` text
,`nazwisko_nauczyciel` text
);

-- --------------------------------------------------------

--
-- Struktura widoku `aktywni_nauczyciele`
--
DROP TABLE IF EXISTS `aktywni_nauczyciele`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `aktywni_nauczyciele`  AS  select `dane_nauczyciel`.`id_nauczyciela` AS `id_nauczyciela`,`dane_nauczyciel`.`imie_nauczyciel` AS `imie_nauczyciel`,`dane_nauczyciel`.`nazwisko_nauczyciel` AS `nazwisko_nauczyciel`,`dane_nauczyciel`.`nr tel` AS `nr tel`,`dane_nauczyciel`.`adres` AS `adres`,`dane_nauczyciel`.`specjalnosc` AS `specjalnosc`,`dane_nauczyciel`.`aktywny` AS `aktywny` from `dane_nauczyciel` where (`dane_nauczyciel`.`aktywny` = 1) ;

-- --------------------------------------------------------

--
-- Struktura widoku `aktywni_uczniowie`
--
DROP TABLE IF EXISTS `aktywni_uczniowie`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `aktywni_uczniowie`  AS  select `dane_uczen`.`imie_uczen` AS `imie_uczen`,`dane_uczen`.`nazwisko_uczen` AS `nazwisko_uczen` from `dane_uczen` where (`dane_uczen`.`aktywny` = 1) ;

-- --------------------------------------------------------

--
-- Struktura widoku `klasa_a`
--
DROP TABLE IF EXISTS `klasa_a`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `klasa_a`  AS  select `klasy`.`nazwa_klasy` AS `nazwa_klasy`,`klasy`.`rocznik` AS `rocznik`,`dane_uczen`.`imie_uczen` AS `imie_uczen`,`dane_uczen`.`nazwisko_uczen` AS `nazwisko_uczen` from ((`uczen_klasa` join `dane_uczen` on((`uczen_klasa`.`id_ucznia` = `dane_uczen`.`id_ucznia`))) join `klasy` on((`uczen_klasa`.`id_klasy` = `klasy`.`id_klasy`))) where (`klasy`.`id_klasy` = 1) ;

-- --------------------------------------------------------

--
-- Struktura widoku `klasa_b`
--
DROP TABLE IF EXISTS `klasa_b`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `klasa_b`  AS  select `klasy`.`nazwa_klasy` AS `nazwa_klasy`,`klasy`.`rocznik` AS `rocznik`,`dane_uczen`.`imie_uczen` AS `imie_uczen`,`dane_uczen`.`nazwisko_uczen` AS `nazwisko_uczen` from ((`uczen_klasa` join `dane_uczen` on((`uczen_klasa`.`id_ucznia` = `dane_uczen`.`id_ucznia`))) join `klasy` on((`uczen_klasa`.`id_klasy` = `klasy`.`id_klasy`))) where (`klasy`.`id_klasy` = 2) ;

-- --------------------------------------------------------

--
-- Struktura widoku `klasa_c`
--
DROP TABLE IF EXISTS `klasa_c`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `klasa_c`  AS  select `klasy`.`nazwa_klasy` AS `nazwa_klasy`,`klasy`.`rocznik` AS `rocznik`,`dane_uczen`.`imie_uczen` AS `imie_uczen`,`dane_uczen`.`nazwisko_uczen` AS `nazwisko_uczen` from ((`uczen_klasa` join `dane_uczen` on((`uczen_klasa`.`id_ucznia` = `dane_uczen`.`id_ucznia`))) join `klasy` on((`uczen_klasa`.`id_klasy` = `klasy`.`id_klasy`))) where (`klasy`.`id_klasy` = 3) ;

-- --------------------------------------------------------

--
-- Struktura widoku `klasa_d`
--
DROP TABLE IF EXISTS `klasa_d`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `klasa_d`  AS  select `klasy`.`nazwa_klasy` AS `nazwa_klasy`,`klasy`.`rocznik` AS `rocznik`,`dane_uczen`.`imie_uczen` AS `imie_uczen`,`dane_uczen`.`nazwisko_uczen` AS `nazwisko_uczen` from ((`uczen_klasa` join `dane_uczen` on((`uczen_klasa`.`id_ucznia` = `dane_uczen`.`id_ucznia`))) join `klasy` on((`uczen_klasa`.`id_klasy` = `klasy`.`id_klasy`))) where (`klasy`.`id_klasy` = 4) ;

-- --------------------------------------------------------

--
-- Struktura widoku `newsletter_tresc`
--
DROP TABLE IF EXISTS `newsletter_tresc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `newsletter_tresc`  AS  select `klasy`.`nazwa_klasy` AS `nazwa_klasy`,`newsletter`.`tresc` AS `tresc`,`newsletter`.`data` AS `data`,`dane_nauczyciel`.`imie_nauczyciel` AS `imie_nauczyciel`,`dane_nauczyciel`.`nazwisko_nauczyciel` AS `nazwisko_nauczyciel` from ((`newsletter` join `klasy` on((`newsletter`.`id_klasy` = `klasy`.`id_klasy`))) join `dane_nauczyciel` on((`newsletter`.`id_nauczyciela` = `dane_nauczyciel`.`id_nauczyciela`))) ;

-- --------------------------------------------------------

--
-- Struktura widoku `nieaktywni_nauczyciele`
--
DROP TABLE IF EXISTS `nieaktywni_nauczyciele`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `nieaktywni_nauczyciele`  AS  select `dane_nauczyciel`.`id_nauczyciela` AS `id_nauczyciela`,`dane_nauczyciel`.`imie_nauczyciel` AS `imie_nauczyciel`,`dane_nauczyciel`.`nazwisko_nauczyciel` AS `nazwisko_nauczyciel`,`dane_nauczyciel`.`nr tel` AS `nr tel`,`dane_nauczyciel`.`adres` AS `adres`,`dane_nauczyciel`.`specjalnosc` AS `specjalnosc`,`dane_nauczyciel`.`aktywny` AS `aktywny` from `dane_nauczyciel` where (`dane_nauczyciel`.`aktywny` = 0) ;

-- --------------------------------------------------------

--
-- Struktura widoku `nieaktywni_uczniowie`
--
DROP TABLE IF EXISTS `nieaktywni_uczniowie`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `nieaktywni_uczniowie`  AS  select `dane_uczen`.`imie_uczen` AS `imie_uczen`,`dane_uczen`.`nazwisko_uczen` AS `nazwisko_uczen` from `dane_uczen` where (`dane_uczen`.`aktywny` = 0) ;

-- --------------------------------------------------------

--
-- Struktura widoku `oceny_uczen`
--
DROP TABLE IF EXISTS `oceny_uczen`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `oceny_uczen`  AS  select `dane_uczen`.`imie_uczen` AS `imie_uczen`,`dane_uczen`.`nazwisko_uczen` AS `nazwisko_uczen`,`oceny`.`ocena` AS `ocena`,`oceny`.`waga` AS `waga`,`oceny`.`komentarz` AS `komentarz`,`przedmioty`.`nazwa` AS `nazwa` from ((`dane_uczen` join `oceny`) join `przedmioty`) where ((`dane_uczen`.`id_ucznia` = `oceny`.`id_ucznia`) and (`oceny`.`id_przedmiotu` = `przedmioty`.`id_przedmiotu`)) ;

-- --------------------------------------------------------

--
-- Struktura widoku `opiekunowie_uczniow`
--
DROP TABLE IF EXISTS `opiekunowie_uczniow`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `opiekunowie_uczniow`  AS  select `dane_uczen`.`imie_uczen` AS `imie_uczen`,`dane_uczen`.`nazwisko_uczen` AS `nazwisko_uczen`,`dane_rodzic`.`imie_rodzica` AS `imie_rodzica`,`dane_rodzic`.`nazwisko_rodzica` AS `nazwisko_rodzica`,`dane_rodzic`.`nr_tel_rodzica` AS `nr_tel_rodzica` from ((`uczen_rodzic` join `dane_uczen` on((`uczen_rodzic`.`id_ucznia` = `dane_uczen`.`id_ucznia`))) join `dane_rodzic` on((`uczen_rodzic`.`id_rodzica` = `dane_rodzic`.`id_rodzica`))) ;

-- --------------------------------------------------------

--
-- Struktura widoku `uwagi_z_danymi`
--
DROP TABLE IF EXISTS `uwagi_z_danymi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `uwagi_z_danymi`  AS  select `dane_uczen`.`imie_uczen` AS `imie_uczen`,`dane_uczen`.`nazwisko_uczen` AS `nazwisko_uczen`,`uwagi`.`tresc` AS `tresc`,`uwagi`.`data_wystawienia` AS `data_wystawienia`,`dane_nauczyciel`.`imie_nauczyciel` AS `imie_nauczyciel`,`dane_nauczyciel`.`nazwisko_nauczyciel` AS `nazwisko_nauczyciel` from ((`uwagi` join `dane_nauczyciel` on((`uwagi`.`id_nauczyciela` = `dane_nauczyciel`.`id_nauczyciela`))) join `dane_uczen` on((`uwagi`.`id_ucznia` = `dane_uczen`.`id_ucznia`))) ;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `dane_nauczyciel`
--
ALTER TABLE `dane_nauczyciel`
  ADD PRIMARY KEY (`id_nauczyciela`);

--
-- Indeksy dla tabeli `dane_rodzic`
--
ALTER TABLE `dane_rodzic`
  ADD PRIMARY KEY (`id_rodzica`);

--
-- Indeksy dla tabeli `dane_uczen`
--
ALTER TABLE `dane_uczen`
  ADD PRIMARY KEY (`id_ucznia`);

--
-- Indeksy dla tabeli `klasa_przedmiot`
--
ALTER TABLE `klasa_przedmiot`
  ADD PRIMARY KEY (`id_klasa_przedmiot`),
  ADD KEY `id_klasy` (`id_klasy`),
  ADD KEY `id_nauczyciela` (`id_nauczyciela`),
  ADD KEY `id_przedmiotu` (`id_przedmiotu`);

--
-- Indeksy dla tabeli `klasy`
--
ALTER TABLE `klasy`
  ADD PRIMARY KEY (`id_klasy`),
  ADD KEY `id_wychowawcy` (`id_wychowawcy`);

--
-- Indeksy dla tabeli `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id_newsletteru`),
  ADD KEY `id_klasy` (`id_klasy`),
  ADD KEY `id_nauczyciela` (`id_nauczyciela`);

--
-- Indeksy dla tabeli `oceny`
--
ALTER TABLE `oceny`
  ADD PRIMARY KEY (`id_oceny`),
  ADD KEY `id_nauczyciela` (`id_nauczyciela`),
  ADD KEY `id_ucznia` (`id_ucznia`),
  ADD KEY `id_przedmiotu` (`id_przedmiotu`);

--
-- Indeksy dla tabeli `przedmioty`
--
ALTER TABLE `przedmioty`
  ADD PRIMARY KEY (`id_przedmiotu`);

--
-- Indeksy dla tabeli `uczen_klasa`
--
ALTER TABLE `uczen_klasa`
  ADD PRIMARY KEY (`id_uczen_klasa`),
  ADD KEY `id_ucznia` (`id_ucznia`),
  ADD KEY `id_klasy` (`id_klasy`);

--
-- Indeksy dla tabeli `uczen_rodzic`
--
ALTER TABLE `uczen_rodzic`
  ADD PRIMARY KEY (`id_wspolne`),
  ADD KEY `id_rodzica` (`id_rodzica`),
  ADD KEY `id_ucznia` (`id_ucznia`);

--
-- Indeksy dla tabeli `uwagi`
--
ALTER TABLE `uwagi`
  ADD PRIMARY KEY (`id_uwagi`),
  ADD KEY `id_nauczyciela` (`id_nauczyciela`),
  ADD KEY `id_ucznia` (`id_ucznia`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `dane_nauczyciel`
--
ALTER TABLE `dane_nauczyciel`
  MODIFY `id_nauczyciela` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT dla tabeli `dane_rodzic`
--
ALTER TABLE `dane_rodzic`
  MODIFY `id_rodzica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT dla tabeli `dane_uczen`
--
ALTER TABLE `dane_uczen`
  MODIFY `id_ucznia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT dla tabeli `klasa_przedmiot`
--
ALTER TABLE `klasa_przedmiot`
  MODIFY `id_klasa_przedmiot` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `klasy`
--
ALTER TABLE `klasy`
  MODIFY `id_klasy` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `oceny`
--
ALTER TABLE `oceny`
  MODIFY `id_oceny` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT dla tabeli `przedmioty`
--
ALTER TABLE `przedmioty`
  MODIFY `id_przedmiotu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `uczen_klasa`
--
ALTER TABLE `uczen_klasa`
  MODIFY `id_uczen_klasa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT dla tabeli `uczen_rodzic`
--
ALTER TABLE `uczen_rodzic`
  MODIFY `id_wspolne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT dla tabeli `uwagi`
--
ALTER TABLE `uwagi`
  MODIFY `id_uwagi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `klasa_przedmiot`
--
ALTER TABLE `klasa_przedmiot`
  ADD CONSTRAINT `klasa_przedmiot_ibfk_1` FOREIGN KEY (`id_klasy`) REFERENCES `klasy` (`id_klasy`),
  ADD CONSTRAINT `klasa_przedmiot_ibfk_2` FOREIGN KEY (`id_nauczyciela`) REFERENCES `dane_nauczyciel` (`id_nauczyciela`),
  ADD CONSTRAINT `klasa_przedmiot_ibfk_3` FOREIGN KEY (`id_przedmiotu`) REFERENCES `przedmioty` (`id_przedmiotu`);

--
-- Ograniczenia dla tabeli `klasy`
--
ALTER TABLE `klasy`
  ADD CONSTRAINT `klasy_ibfk_1` FOREIGN KEY (`id_wychowawcy`) REFERENCES `dane_nauczyciel` (`id_nauczyciela`),
  ADD CONSTRAINT `klasy_ibfk_2` FOREIGN KEY (`id_klasy`) REFERENCES `uczen_klasa` (`id_klasy`);

--
-- Ograniczenia dla tabeli `newsletter`
--
ALTER TABLE `newsletter`
  ADD CONSTRAINT `newsletter_ibfk_1` FOREIGN KEY (`id_nauczyciela`) REFERENCES `dane_nauczyciel` (`id_nauczyciela`),
  ADD CONSTRAINT `newsletter_ibfk_2` FOREIGN KEY (`id_klasy`) REFERENCES `klasy` (`id_klasy`),
  ADD CONSTRAINT `newsletter_ibfk_3` FOREIGN KEY (`id_nauczyciela`) REFERENCES `dane_nauczyciel` (`id_nauczyciela`);

--
-- Ograniczenia dla tabeli `oceny`
--
ALTER TABLE `oceny`
  ADD CONSTRAINT `oceny_ibfk_1` FOREIGN KEY (`id_nauczyciela`) REFERENCES `dane_nauczyciel` (`id_nauczyciela`),
  ADD CONSTRAINT `oceny_ibfk_2` FOREIGN KEY (`id_ucznia`) REFERENCES `dane_uczen` (`id_ucznia`),
  ADD CONSTRAINT `oceny_ibfk_3` FOREIGN KEY (`id_przedmiotu`) REFERENCES `przedmioty` (`id_przedmiotu`);

--
-- Ograniczenia dla tabeli `uczen_klasa`
--
ALTER TABLE `uczen_klasa`
  ADD CONSTRAINT `uczen_klasa_ibfk_1` FOREIGN KEY (`id_ucznia`) REFERENCES `dane_uczen` (`id_ucznia`),
  ADD CONSTRAINT `uczen_klasa_ibfk_2` FOREIGN KEY (`id_klasy`) REFERENCES `klasy` (`id_klasy`);

--
-- Ograniczenia dla tabeli `uczen_rodzic`
--
ALTER TABLE `uczen_rodzic`
  ADD CONSTRAINT `uczen_rodzic_ibfk_1` FOREIGN KEY (`id_rodzica`) REFERENCES `dane_rodzic` (`id_rodzica`),
  ADD CONSTRAINT `uczen_rodzic_ibfk_2` FOREIGN KEY (`id_ucznia`) REFERENCES `dane_uczen` (`id_ucznia`);

--
-- Ograniczenia dla tabeli `uwagi`
--
ALTER TABLE `uwagi`
  ADD CONSTRAINT `uwagi_ibfk_1` FOREIGN KEY (`id_nauczyciela`) REFERENCES `dane_nauczyciel` (`id_nauczyciela`),
  ADD CONSTRAINT `uwagi_ibfk_2` FOREIGN KEY (`id_ucznia`) REFERENCES `dane_uczen` (`id_ucznia`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
