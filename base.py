import pymysql.cursors
import subprocess, datetime

def create_connection(name='', cursorclass=pymysql.cursors.Cursor):
	return pymysql.connect(
		host="127.0.0.1",
		user="root",
		password="ja",
		db='',
		charset='utf8mb4',
		cursorclass=cursorclass
	)

class Database:
	def __init__(self, name, connection):
		self.database_name = name
		self.connection = connection
		self.commands = []

	def create_database(self):
		with self.connection.cursor() as cursor:
			cursor.execute('SHOW DATABASES')
			if not any(x for x in cursor.fetchall() if self.database_name in x):
				cursor.execute(f'CREATE DATABASE {self.database_name}')
				#print(f'Database "{self.database_name}" created.')
			else:
				pass
				#print(f'Database "{self.database_name}" already exists.')
			cursor.execute(f'USE {self.database_name}')
			for line in self.commands[:-1]:
				cursor.execute(str(line))
			self.connection.commit()
			print("READY")

	def drop_database(self):
		with self.connection.cursor() as cursor:
			try:
				cursor.execute(f'DROP DATABASE {self.database_name}')
				#print(f'Database "{self.database_name}" deleted.')
			except pymysql.err.InternalError:
				return 0

	def load_sql_script(self, name):
		with open(name, "r") as script_file:
			single_command = ''
			for line in script_file.readlines():
				if not line.startswith("--") and not line.startswith("\n") and not line.startswith("/"): #reading line
					if not line.endswith(';\n'):
						single_command+=line[:-1]
					elif line.endswith(';\n'):
						single_command+=line[:-1]
						self.commands.append(single_command)
						single_command = ''
				else:
					continue

	def get_grades(self, surname):			#TODO ID STUDENTA
		with self.connection.cursor() as cursor:
			print((f"SELECT id_ucznia FROM dane_uczen WHERE dane_uczen.nazwisko_uczen=\"{surname}\""))
			id_u = cursor.execute(f"SELECT id_ucznia FROM dane_uczen WHERE nazwisko_uczen=\"{surname}\"")
			print("id_ucznia : "+str(id_u))
			print(f"SELECT przedmioty.nazwa, dane_nauczyciel.nazwisko_nauczyciel, oceny.data_wystawienia, oceny.waga, oceny.ocena, oceny.komentarz FROM przedmioty,dane_nauczyciel,oceny WHERE id_ucznia=\"{id_u}\" and dane_nauczyciel.id_nauczyciela=oceny.id_nauczyciela and przedmioty.id_przedmiotu=oceny.id_przedmiotu")
			sql = f"SELECT przedmioty.nazwa, dane_nauczyciel.nazwisko_nauczyciel, oceny.data_wystawienia, oceny.waga, oceny.ocena, oceny.komentarz FROM przedmioty,dane_nauczyciel,oceny WHERE id_ucznia=\"{id_u}\" and dane_nauczyciel.id_nauczyciela=oceny.id_nauczyciela and przedmioty.id_przedmiotu=oceny.id_przedmiotu"
			cursor.execute(sql)
			result = cursor.fetchone()
			print(result)

	def get_news(self):
		with self.connection.cursor() as cursor:
			cursor.execute("SELECT klasy.nazwa_klasy, dane_nauczyciel.nazwisko_nauczyciel, newsletter.tresc, newsletter.data FROM dane_nauczyciel, klasy, newsletter WHERE dane_nauczyciel.id_nauczyciela=newsletter.id_nauczyciela and klasy.id_klasy=newsletter.id_klasy")
			result = cursor.fetchall()
			print(result)

	def print_selected_class(self, selected_class):
		with self.connection.cursor() as cursor:
			cursor.execute("SELECT * FROM klasa_"+str(selected_class))
			result = cursor.fetchall()
			print(result)

	#TODO
	def add_grade(self,grade, teacher_name, teacher_surname, class_, student_name, student_surname, faculty, weight, comment):
		with self.connection.cursor() as cursor:
			id_teacher = self.teacher_id(teacher_name, teacher_surname)
			id_student = self.students_id(student_name, student_surname)
			id_faculty = self.faculty_id(faculty)
			#cursor.execute(f"INSERT INTO oceny (id_oceny, id_ucznia, id_nauczyciela, id_przedmiotu, ocena, data_wystawienia, waga, komentarz) VALUES(NULL, \"{id_student}\", \"{id_teacher}\", \"{id_faculty}\", \"{grade}\", \"{datetime.datetime.now()}\", \"{weight}\", \"{comment}\")")
		self.connection.commit()

	def add_news(self, content, teacher_name, teacher_surname, class_):
		with self.connection.cursor() as cursor:
			date = datetime.date.today()
			id_teacher = self.teacher_id(teacher_name, teacher_surname)
			cursor.execute(f"INSERT INTO newsletter (id_newsletteru, id_klasy, id_nauczyciela, tresc, data) VALUES (3, 1, \"{id_teacher}\", \"{content}\", \"{date}\")")
		self.connection.commit()

	def faculty_id(self, faculty):
		with self.connection.cursor() as cursor:
			cursor.execute(f"SELECT id_przedmiotu FROM przedmioty WHERE nazwa = \"{faculty}\"")
			id_faculty = cursor.fetchone()
			return id_faculty

	def students_id(self,student_name, student_surname):
		with self.connection.cursor() as cursor:
			cursor.execute(f"SELECT id_ucznia FROM dane_uczen WHERE nazwisko_uczen=\"{student_surname}\" AND imie_uczen=\"{student_name}\"")
			id_student = cursor.fetchone()
			return id_student

	def teacher_id(self,teacher_name, teacher_surname):
		with self.connection.cursor() as cursor:
			cursor.execute(f"SELECT id_nauczyciela FROM dane_nauczyciel WHERE imie_nauczyciel = \"{teacher_name}\" AND nazwisko_nauczyciel = \"{teacher_surname}\"")
			teacher_id = cursor.fetchone()
			return teacher_id[0]

	def get_faculty_grades(self,teacher_name, teacher_surname): #TODO
		teacher_id = self.teacher_id(teacher_name, teacher_surname)
		with self.connection.cursor() as cursor:
			print(f"SELECT dane_uczen.imie_uczen, dane_uczen.nazwisko_uczen, oceny.ocena, oceny.data_wystawienia, oceny.waga, oceny.komentarz FROM dane_uczen, oceny WHERE oceny.id_nauczyciela=\"{teacher_id}\" and oceny.id_ucznia=dane_uczen.id_ucznia")
			result = cursor.fetchall()
			print(result)

	def add_raport(self,student_name, student_surname, content, name_teacher, surname_teacher): #TODO
		with self.connection.cursor() as cursor:
			id_teacher = self.teacher_id(name_teacher, surname_teacher)
			id_student = self.students_id(student_name, student_surname)
			cursor.execute(f"INSERT INTO uwagi (id_uwagi, id_nauczyciela, id_ucznia, tresc ,data_wystawienia) VALUES (2, \"{id_teacher}\", \"{id_student}\", \"{content}\", \"{datetime.date.today()}\")")
		self.connection.commit()




if __name__ == "__main_":
	connection = create_connection()
	base = Database("dziennik", connection)
	base.drop_database()
	base.load_sql_script("dziennik_szkolny.sql")
	base.create_database()